import glob
import pandas as pd
import os
import xml.etree.ElementTree as ET


fld = input("Folder name: ")

for file in os.listdir():
    ro,ex = os.path.splitext(file)
        
    if(ex == ".xml"):   
    
        tree = ET.parse(file)
        root = tree.getroot()
        root.find('folder').text = fld
        root.find('filename').text = file
        root.find('path').text = os.getcwd()
        print(root.find('path').text)
        
        tree.write(file)
        print("completed")